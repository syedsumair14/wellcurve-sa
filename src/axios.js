import axios from 'axios';
const BASE_URL = "http://wellcurveproject.n1.iworklab.com:3206/";


const client = axios.create({ baseURL: BASE_URL, json: true });

export default {
  async execute(method, resource, data) {
    // inject the accessToken for each request
    let accessToken = localStorage.getItem('user-token');
    return client({
      method,
      url: resource,
      data,
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })
  },

  get(url) {
    return this.execute('get', BASE_URL + url)
  },

  post(url, data) {
    return this.execute('post', BASE_URL + url, data)
  },

  put(url, data) {
    return this.execute('put', BASE_URL + url, data);
  },

  delete(url) {
    return this.execute('delete', BASE_URL + url)
  }
}