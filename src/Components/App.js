import React from 'react';
import Login from './LoginPage';
import ResponsiveDrawer from './Sidebar'

import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';




// Private Route Auth 
export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => {
    let bool = localStorage.getItem("user-token") ? true: false;
    return (bool)   
    ? 
    <Component {...props} />     
    : <Redirect to="/login"/>
    }
    } />
    )

const App = () => {
    return(
        <div>
            <Router>
                <Switch>
                    <Route exact path='/login' component={Login} />
                    <PrivateRoute path='/' component={ResponsiveDrawer} />
                </Switch>
            </Router>
        </div>
    )
}

export default App;