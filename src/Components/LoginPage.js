import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import axios from '../axios';
import Loader from 'react-loader-spinner';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import '../Styles/login.css'

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});





class SignIn extends React.Component{
    constructor(props){
        super(props);
        this.state={
            email: '',
            password: '',
            showLoader: false,
        }
    }

submitForm = (event) => {
    event.preventDefault();
    this.setState({
        showLoader: true
    });
    let body = {
        "email": this.state.email,
        "password": this.state.password
      }
      console.log(body)
    axios.post('login', body)
         .then(response => {
            if(response.status == 200){
                this.setState({
                    showLoader: false
                })
                toast.success(response.data.message)
                localStorage.setItem('user-token', response.data.token)
                localStorage.getItem('user-token') ? this.props.history.push('/') : this.props.history.push('/login')
               }
        })
        .catch(err =>  {this.setState({
            showLoader: false
        })
        console.log(err)
        toast.error(err.message)
        }
        )
}

setEmail = (event) => {
    this.setState({
        email: event.target.value
    })
}

setPassword = (event) => {
    this.setState({
        password: event.target.value
    })
}

forgotPassword = () => {
    alert("clicked")
}

  
render(){
    const { classes } = this.props;
  return (
    <div>
        <ToastContainer autoClose={2000} position={"top-left"} />
    <main className={classes.main}>
        <div className="loader">
            {this.state.showLoader ?  <Loader
            type="Puff"
            color="#00BFFF"
            height="100"	
            width="100"
        />  : null }
           
        </div>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email Address</InputLabel>
            <Input onChange={this.setEmail} id="email" name="email" autoComplete="email" autoFocus />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input onChange={this.setPassword} name="password" type="password" id="password" autoComplete="current-password" />
          </FormControl>
          <Button
          onClick={this.forgotPassword}
          className="submit"

          >
            Forgot Password?
          </Button>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={this.submitForm}
          >
            Sign in
          </Button>
        </form>
      </Paper>
    </main>

    {/* FORGOT MODAL */}
    
    </div>
  )
}
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);